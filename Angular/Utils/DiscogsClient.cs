﻿using System.Net.Http;
using System.Threading.Tasks;
using DiscogsViewer.Models;
using Newtonsoft.Json;

namespace DiscogsViewer.Utils
{
    public class DiscogsClient
    {
        private HttpClient DiscogsApiClient { get; set; }

        public DiscogsClient()
        {
            DiscogsApiClient = new HttpClient();
            DiscogsApiClient.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Blood13thDiscogsViewer/0.1 +http://undefined.com");
            DiscogsApiClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Discogs token=WSwiokTypKrClxyrgKlWrBotxlhligbALjddWQpa");
        }

        public async Task<ArtistSearchResult> SearchArtist(string query)
        {
            HttpResponseMessage response = await DiscogsApiClient.GetAsync($"https://api.discogs.com/database/search?q={query}&type=artist");
            ArtistSearchResult res = new ArtistSearchResult();
            if (response.IsSuccessStatusCode)
            {
                var val = await response.Content.ReadAsStringAsync();
                res = JsonConvert.DeserializeObject<ArtistSearchResult>(val);
            }

            return res;
        }
    }
}
